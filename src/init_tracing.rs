use console_subscriber::ConsoleLayer;
use tracing::subscriber::set_global_default;
use tracing_error::ErrorLayer;
use tracing_log::LogTracer;
use tracing_subscriber::filter::Targets;
use tracing_subscriber::fmt::format::FmtSpan;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::Layer;
use tracing_subscriber::Registry;

pub(crate) fn init_tracing() -> Result<(), Box<dyn std::error::Error>> {
    LogTracer::init()?;

    let targets = std::env::var("RUST_LOG")
        .unwrap_or_else(|_| "info".into())
        .parse::<Targets>()?;

    let format_layer = tracing_subscriber::fmt::layer()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_filter(targets);

    let console_layer = ConsoleLayer::builder()
        .with_default_env()
        .event_buffer_capacity(1024 * 1024)
        .server_addr(([0, 0, 0, 0], 6669))
        .spawn();

    let subscriber = Registry::default()
        .with(format_layer)
        .with(console_layer)
        .with(ErrorLayer::default());

    set_global_default(subscriber)?;

    Ok(())
}
