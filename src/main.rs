use axum::response::IntoResponse;
use axum::Router;
use axum_liveview::associated_data::FormEventValue;
use axum_liveview::html;
use axum_liveview::pubsub::InProcess;
use axum_liveview::EmbedLiveView;
use axum_liveview::Html;
use axum_liveview::LiveView;
use tower::limit::ConcurrencyLimitLayer;
use tower::ServiceBuilder;
use tower_http::trace::DefaultMakeSpan;
use tower_http::trace::TraceLayer;
use tracing::Level;

mod init_tracing;
// mod tea;

#[derive(serde::Deserialize, serde::Serialize, PartialEq)]
enum Msg {
    Red,
    Green,
    Blue,
}

#[derive(Default)]
struct State {
    red: u8,
    green: u8,
    blue: u8,
}

fn color_input<M>(title: &str, msg: M) -> Html<M> {
    html! {
        <label style="display: flex; justify-content: space-between; padding: 8px; width: 100%;">
            <span style="font-weight: 500;">{ title }</span>
            <input type="number" min="0" max="255" axm-input={ msg }/>
        </label>
    }
}

#[axum::async_trait]
impl LiveView for State {
    type Message = Msg;

    async fn update(self, msg: Self::Message, data: axum_liveview::AssociatedData) -> Self {
        let value = if let Some(FormEventValue::String(ref string)) = data.as_form() {
            if let Ok(value) = string.parse() {
                value
            } else {
                return self;
            }
        } else {
            return self;
        };

        match msg {
            Msg::Red => Self { red: value, ..self },
            Msg::Green => Self {
                green: value,
                ..self
            },
            Msg::Blue => Self {
                blue: value,
                ..self
            },
        }
    }

    fn render(&self) -> Html<Self::Message> {
        html! {
            <div style="margin: 0 auto; width: 350px; padding: 16px; background-color: #f5f5f5; border: 1px solid #e5e5e5; border-radius: 4px;">
                { color_input("Red", Msg::Red) }
                { color_input("Green", Msg::Green) }
                { color_input("Blue", Msg::Blue) }
                <div style={
                    format!(
                        "height: 300px; width: 300px; margin: 15px auto 0; border-radius: 4px; background-color: rgb({}, {}, {})",
                        self.red, self.green, self.blue
                    )
                }></div>
            </div>
        }
    }
}

async fn root(live: EmbedLiveView) -> impl IntoResponse {
    html! {
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8" />
                <title>"Hewwo Mr Obama"</title>
                { axum_liveview::assets() }
            </head>
            <body style="margin: 0; background-color: #222;">
                <div style="display: flex; align-items: center; justify-content: space-around; min-height: 100vh;">
                    {live.embed(State::default())}
                </div>
                <script>
                    r#"
                        const liveView = new LiveView({ host: 'localhost', port: 3000 });
                        liveView.connect();
                    "#
                </script>
            </body>
        </html>
    }
}

fn construct_router() -> Router {
    let pubsub = InProcess::new();

    let global_middlewares = ServiceBuilder::new()
        .layer(axum_liveview::layer(pubsub))
        .layer(TraceLayer::new_for_http().make_span_with(DefaultMakeSpan::new().level(Level::INFO)))
        .layer(ConcurrencyLimitLayer::new(64));

    Router::new()
        .route("/", axum::routing::get(root))
        .merge(axum_liveview::routes())
        .layer(global_middlewares)
}

#[tracing::instrument]
async fn run() -> Result<(), Box<dyn std::error::Error>> {
    axum::Server::bind(&([0, 0, 0, 0], 3000).into())
        .serve(construct_router().into_make_service())
        .await?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    init_tracing::init_tracing()?;

    run().await
}
